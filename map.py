#!/usr/bin/env python
import sys
import numpy as np
import numpy.fft as npf
import scipy.stats as spst
import math

# A function to easily print an array in a formatted manner
def printArray(toprint):
  for i, val in enumerate(toprint):
    if math.isnan(val): val = 0;
    if i < (len(toprint) - 1):
      sys.stdout.write(str(val))
      sys.stdout.write(",")
    else:
      sys.stdout.write(str(val))

for line in sys.stdin:
  line = line.strip(" []\t\n")
  data = line.split("\t")
  eeg_readings = data[0].strip(" []\n\t")
  arrays = eeg_readings.split(",")

  # Arrays to keep our features
  max_values = []
  min_values = []
  mean_values = []
  var_values = []
  mobility_values = []
  complexity_values = []

  # Recreate the matrix in python
  matrix = []

  # For each row of readings...
  for i, val in enumerate(arrays):

    # Get values for each row and convert them to floating numbers (from string)
    val_array = val.strip(" []\n\t").split(":")
    values = map(float,val_array)
    matrix.append(values)

    #compute various metrics and store them in the corresponding arrays
    max_values.append(np.amax(values))
    min_values.append(np.amin(values))
    mean_values.append(np.mean(values))
    tmp_variance = np.var(values);
    var_values.append(tmp_variance)

    values1 = values[0:len(values)-1]
    values2 = values[1:len(values)]

    dV = np.subtract(values1, values2)
    dv_variance = np.var(dV)

    # The mobility represents the average difference between two consecutive
    # values divided by the variance of the values
    mobility = 0
    if tmp_variance != 0 :
      mobility = np.sqrt(np.absolute(dv_variance / tmp_variance))
    mobility_values.append(mobility)

    dv1 = dV[0:len(dV)-1]
    dv2 = dV[1:len(dV)]

    # The complexity represents the average difference between two consecutive
    # values divided by the dv_variance
    second_degree_dv = np.subtract(dv1,dv2)
    sddv_variance = np.var(second_degree_dv)
    complexity = 0
    if dv_variance != 0 and mobility != 0 :
      second_d_mobility = np.sqrt(np.absolute(sddv_variance / dv_variance))
      complexity = second_d_mobility / mobility
    complexity_values.append(complexity)

# Print the features
# The mapreduce expects a key and a value...it is best if we have a key
sys.stdout.write(data[3])
sys.stdout.write("\t")
printArray(max_values)
sys.stdout.write(",")
printArray(min_values)
sys.stdout.write(",")
printArray(mean_values)
sys.stdout.write(",")
printArray(var_values)
sys.stdout.write(",")
printArray(mobility_values)
sys.stdout.write(",")
printArray(complexity_values)
sys.stdout.write(",")
sys.stdout.write(data[1])
sys.stdout.write(",")
sys.stdout.write(data[2])
# sys.stdout.write(",")
# sys.stdout.write(data[4]) data[4] = sequence number (not really important as not present in the test data)
sys.stdout.write("\n")