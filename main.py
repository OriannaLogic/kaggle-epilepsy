#!/home/lucas/anaconda/bin/python
# -*- coding: utf-8 -*-

# This scripts takes each file, apply the best classifier that applies to its specificities

import glob, os

# def get_classifier_script(file_name):
#   return

# for i in range(1, 5):
#   os.system("./dog_1_to_4_pipeline.py " + str(i) + " --q")

# os.system("./dog_5_pipeline.py --q")
# os.system("./patient_1_pipeline.py --q")
# os.system("./patient_2_pipeline.py --q")


read_files = glob.glob("results/results_*.txt")

with open("results/submission.txt", "wb") as outfile:
  outfile.write("clip,preictal")
  outfile.write("\n")
  for f in read_files:
    with open(f, "rb") as infile:
      outfile.write(infile.read())