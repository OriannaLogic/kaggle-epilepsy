#!/home/lucas/anaconda/bin/python
# -*- coding: utf-8 -*-

################### Classification for dogs with 15 channels ###########
# dog 1 90 columns -> 15 channels

import sys

if (len(sys.argv) == 2 and sys.argv[1] == "--help"):
  print "This script only serves for dog 5"
  sys.exit(0)

quiet = False
if (len(sys.argv) == 2 and sys.argv[1] == "--q"):
  quiet = True

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import roc_curve, auc
from sklearn.metrics import confusion_matrix
from sklearn.cross_validation import StratifiedKFold




# Import data from dog 1
patient_nb = 1
file_train='processed/patient_' + str(patient_nb) + '.txt' # your feature file
file_test='processed/test_patient_' + str(patient_nb) + '.txt' # the test file
file_pre='results/results_patient_' + str(patient_nb) + '.txt' # where you are going to wite your results

# Read file and seperate the values of the features from the class
data = pd.read_csv(file_train, header = None)
rows, cols = data.shape

# The features X. The first argument of dump selects columns to remove
X = data.drop([0, cols-3, cols-2, cols-1], axis = 1).values

# The class y
y = data[cols-3].values

#load test data
test_data = pd.read_csv(file_test, header=None)
rows,cols = test_data.shape
test_X = test_data.drop([0,cols-3,cols-2,cols-1],axis=1).values
test_y = test_data[0].values
subject = test_data[cols-2].values #the subject
to_predict = test_data[0].values

# This is needed only if we want to plot the roc curve
plt.figure(num=None, figsize=(8, 6), dpi=100, facecolor='w', edgecolor='k')

#the classifier we are going to use
clf = LogisticRegression()

#We evaluate the results with a 10 fold validation
#this create in cv 10 sets of indexes that partition the data to 10 different train-test sets
cv = StratifiedKFold(y, n_folds=10)

# Variables to store statistics
mean_tpr = 0.0 # mean true positive rate

# We are going to evaluate the ROC curve for various false positive rates
# We create values in equal spacing between 0 and 1
# We will use this values to interpolate the curve ...sometimes there are not enough data points for a "nice" curve
mean_fpr = np.linspace(0, 1, 100)

# The cumulative confusion matrix. Here we store the partial confusion matrices from each fold
conf_matrix=np.zeros((2,2))

features_importance = []

# For all partition of the data get the indexes and train on X[train_indexes] and evaluate on X[test_indexes]
for i, (train, test) in enumerate(cv):

  model = clf.fit(X[train], y[train])
  probas_ = model.predict_proba(X[test]) # fit and predict

  fpr, tpr, thresholds = roc_curve(y[test], probas_[:, 1]) # y[test] holds the real values of the fold we are testing on
  # we get a set of true positive  and false positive rates and the corresponding thresholds

  #create a nice interpolation for our predefined spacing
  #and store it so we can have an average over its aggregation
  mean_tpr += np.interp(mean_fpr, fpr, tpr)
  mean_tpr[0] = 0.0 # the value at 0 should be 0 (just in case it is missing)
    #aggregate the confusion matrix with the prediction of each fold
  y_pred = clf.predict(X[test])
  tmp_conf_m=confusion_matrix(y[test],y_pred)
  conf_matrix=np.add(conf_matrix,tmp_conf_m)

  #we could plot all ROC curves for all folds if we want to
  #roc_auc = auc(fpr, tpr)
    #plt.plot(fpr, tpr, lw=1, label='ROC fold %d (area = %0.2f)' % (i, roc_auc))

#compute the mean true positive rate as the mean over the interpolated values
mean_tpr /= len(cv)
mean_tpr[-1] = 1.0 # the last value has to be one
mean_auc = auc(mean_fpr, mean_tpr) # instead of comparing all the plots we can store this value as
#is measures the Area Under the Curve (AUC ROC)

#we can see in the confustion matrix the absolute values of
#classification to various classes
if quiet is not True:
  print conf_matrix

#plot the ROC curve
plt.plot([0, 1], [0, 1], '--', color=(0.6, 0.6, 0.6), label='Luck')
plt.plot(mean_fpr, mean_tpr, 'k--',
         label='Mean ROC (area = %0.2f)' % mean_auc, lw=2)

plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver operating characteristic example')
plt.legend(loc="lower right")
if quiet is not True:
  plt.show()

# assume that somehow you found your best options
# train on the entire dataset
clf.fit(X, y)

predictions=clf.predict_proba(test_X)
# this gives the probability for each class
# you can use clf.predict(test) to just get the class

# Write your predictions
f = open(file_pre,'w')
for idx,val in enumerate(predictions):
    f.write(subject[idx])
    f.write('_')
    f.write(to_predict[idx])
    f.write('.mat,')
    f.write(str(np.round(val[1],decimals=1)))
    f.write('\n')
f.close()