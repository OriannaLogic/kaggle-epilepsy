ENDC = '\033[0m'
OKGREEN = '\033[92m'
WARNING = '\033[93m'
FAIL = '\033[91m'

def print_valid(string):
  print OKGREEN + string + ENDC

def print_warning(string):
  print WARNING + string + ENDC

def print_error(string):
  print FAIL + string + ENDC