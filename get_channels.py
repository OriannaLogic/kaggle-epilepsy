#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import numpy as np
import pandas as pd

print ""
print "DOGS:"
for i in range(1, 6):
  file_train = 'processed/dog_' + str(i) + '.txt' # your features file

  # Read file and seperate the values of the features from the class
  data = pd.read_csv(file_train, header = None)
  rows, cols = data.shape
  print "Dog n°" + str(i) + " has " + str(cols) + " columns and " +  (str((cols - 4) / 6)) + " channels."

print ""
print "PATIENTS:"
for i in range(1, 3):
  file_train = 'processed/patient_' + str(i) + '.txt' # your features file

  # Read file and seperate the values of the features from the class
  data = pd.read_csv(file_train, header = None)
  rows, cols = data.shape
  print "Patient n°" + str(i) + " has " + str(cols) + "columns and " +  (str((cols - 4) / 6)) + " channels."

print ""